#!/bin/sh

__GLX_FORCE_VENDOR_LIBRARY_0=dummy
export __GLX_FORCE_VENDOR_LIBRARY_0

PLATFORM=$(uname -s)
if [ "$PLATFORM" = "Haiku" ]; then
    LIBRARY_PATH="$LIBRARY_PATH:$TOP_BUILDDIR/tests/dummy/.libs"
    export LIBRARY_PATH
elif [ "$PLATFORM" = "Darwin" ]; then
    DYLD_LIBRARY_PATH="$DYLD_LIBRARY_PATH:$TOP_BUILDDIR/tests/dummy/.libs"
    export DYLD_LIBRARY_PATH
else
    LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOP_BUILDDIR/tests/dummy/.libs"
    export LD_LIBRARY_PATH
fi
